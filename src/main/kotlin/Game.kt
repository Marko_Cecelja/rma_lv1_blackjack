import cards.Deck
import players.Computer
import players.Human
import players.Player

class Game(private val players: MutableList<Player>, private val deck: Deck) {

    constructor() : this(
        mutableListOf(
            Human("Player1", true),
            Computer(false)
        ), Deck()
    )

    fun start() {
        for (player in players) {
            var card = deck.cards.iterator().next()
            card.isHidden = false
            player.hand.add(card)
            player.totalPoints += card.value?.defaultPoints!!

            deck.cards.remove(card)

            card = deck.cards.iterator().next()
            if (player !is Computer) {
                card.isHidden = false
                player.totalPoints += card.value?.defaultPoints!!
            }
            player.hand.add(card)
            deck.cards.remove(card)
        }

        printPlayersHands()

        this.players.forEach { it.play(deck) }
    }

    private fun printPlayersHands() {
        players.forEach {
            it.printHand()
        }
    }

    fun checkResult() {
        val computer: Player = players.stream().filter { it is Computer }.findFirst().get()

        players.stream().filter { it !is Computer }.forEach {
            if (it.hasBlackJack) {
                if (computer.hasBlackJack) {
                    println("Tie!")
                } else {
                    println("${it.name} wins!")
                }
            } else {
                when {
                    computer.totalPoints > 21 -> {
                        println("${it.name} wins!")
                    }
                    it.totalPoints == 21 -> {

                        when {
                            computer.hasBlackJack -> {
                                println("Computer wins!")
                            }
                            computer.totalPoints != 21 -> {
                                println("${it.name} wins!")
                            }
                            else -> {
                                println("Tie!")
                            }
                        }
                    }
                    it.totalPoints > computer.totalPoints -> {
                        println("${it.name} wins")
                    }
                    it.totalPoints == computer.totalPoints -> {
                        println("Tie!")
                    }
                    else -> {
                        println("Computer wins!")
                    }
                }
            }
        }
    }
}