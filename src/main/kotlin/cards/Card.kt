package cards

import enums.CardTypeEnum
import enums.CardValueEnum

class Card(val value: CardValueEnum?, val type: CardTypeEnum?, var isHidden: Boolean) {
    override fun toString(): String {
        return "${value?.title} of ${type?.title}"
    }
}