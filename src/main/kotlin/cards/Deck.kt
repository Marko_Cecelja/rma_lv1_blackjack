package cards

import enums.CardValueEnum
import enums.CardTypeEnum

data class Deck(val cards: MutableList<Card>) {

    constructor() : this(mutableListOf<Card>()) {
        for (cardType in CardTypeEnum.values()) {
            for (cardValue in CardValueEnum.values()) {
                this.cards.add(Card(cardValue, cardType, true))
            }
        }
        this.cards.shuffle()
    }
}