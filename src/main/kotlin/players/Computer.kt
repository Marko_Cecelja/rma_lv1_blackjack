package players

import cards.Deck
import enums.CardValueEnum

class Computer(turnToPlay: Boolean) : Player("Computer", mutableListOf(), turnToPlay, 0, false) {

    override fun play(deck: Deck) {
        this.hand.stream().filter { it.isHidden }.findFirst()
            .ifPresent {
                this.totalPoints += it.value?.defaultPoints!!
                it.isHidden = false
            }

        printHand()

        if (checkBlackJack()) {
            this.hasBlackJack = true
            return
        }

        while (this.totalPoints <= 16) {

            val card = deck.cards.iterator().next()
            card.isHidden = false

            deck.cards.remove(card)

            this.hand.add(card)

            if (card.value == CardValueEnum.ACE && (this.totalPoints + card.value.defaultPoints) > 21) {
                this.totalPoints += 1
            } else {
                this.totalPoints += card.value?.defaultPoints!!
            }

            printHand()
        }
    }
}