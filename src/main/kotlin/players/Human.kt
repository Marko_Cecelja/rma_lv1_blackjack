package players

import cards.Deck
import kotlin.system.exitProcess

class Human(name: String, turnToPlay: Boolean) : Player(name, mutableListOf(), turnToPlay, 0, false) {

    override fun play(deck: Deck) {

        if (checkBlackJack()) {
            this.hasBlackJack = true
            return
        }

        println("Hit or Stand? [H/S]")
        var userInput = readLine() ?: "S"

        while (userInput != "S") {

            if (userInput != "H") {
                println("Invalid command! Use 'H' for hit and 'S' for stand.")
                userInput = readLine() ?: "S"
                continue
            }

            hit(deck)
            this.printHand()

            if (this.totalPoints > 21) {
                println("Computer wins!")
                exitProcess(0)
            } else if (this.totalPoints == 21) {
                stand()
            }

            println("Hit or Stand? [H/S]")
            userInput = readLine() ?: "S"
        }
        stand()
    }

    private fun hit(deck: Deck) {
        val card = deck.cards.iterator().next()
        card.isHidden = false

        deck.cards.remove(card)

        this.hand.add(card)
        this.totalPoints += card.value?.defaultPoints!!
    }

    private fun stand() {
        this.turnToPlay = false
    }
}