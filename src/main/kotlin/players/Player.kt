package players

import cards.Card
import cards.Deck
import enums.CardValueEnum

abstract class Player(
    val name: String,
    val hand: MutableList<Card>,
    var turnToPlay: Boolean,
    var totalPoints: Int,
    var hasBlackJack: Boolean
) {

    abstract fun play(deck: Deck)

    fun printHand() {
        println("${name}:\n${hand.filter { card -> !card.isHidden }}\nTotal points: $totalPoints\n")
    }

    fun checkBlackJack(): Boolean {
        return this.totalPoints == 21 && this.hand.stream().anyMatch { it.value == CardValueEnum.ACE } &&
                this.hand.stream()
                    .anyMatch { it.value == CardValueEnum.TEN || it.value == CardValueEnum.JACK || it.value == CardValueEnum.QUEEN || it.value == CardValueEnum.KING }
    }
}